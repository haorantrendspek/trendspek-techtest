## Requirements
* PHP 7+
* npm
* Composer

## Instructions

 * `npm install`
 * `composer install`
 * Copy `.env.example` to `.env`. You will need this file shortly.
 * `php -s localhost:8000`
 * Open this in your browser: `http://localhost:8000`
 * Keep following these instructions.
